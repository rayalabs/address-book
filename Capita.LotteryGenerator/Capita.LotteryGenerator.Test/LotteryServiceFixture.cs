﻿using System;
using System.Collections.Generic;
using System.Linq;
using Capita.LotteryGenerator.Service;
using Moq;
using FluentAssertions;
using NUnit.Framework;

namespace Capita.LotteryGenerator.Test
{
    [TestFixture]
    public class LotteryServiceFixture
    {
        private ILotteryService lotteryService;
        private Mock<IStorageService> storageService;
        private Mock<IColorPresenter> colorPresenter;
        private Mock<INumberGenerator> numberGenerator;
        private List<int> numberSet;
        private Dictionary<int, ColorType> colorMapping;

        [SetUp]
        public void Setup()
        {
            storageService = new Mock<IStorageService>();
            colorPresenter = new Mock<IColorPresenter>();
            numberGenerator = new Mock<INumberGenerator>();
            lotteryService = new LotteryService(numberGenerator.Object, storageService.Object, colorPresenter.Object);

            numberSet = new List<int> { 1, 19, 28 };
            colorMapping = new Dictionary<int, ColorType> { { 1, ColorType.Red }, { 19, ColorType.Blue }, { 28, ColorType.Green } };
        }

        [Test]
        public void Should_call_storage_service_for_generated_set()
        {
            numberGenerator.Setup(x => x.GenerateNumberSet(6, 49 )).Returns(numberSet);

            colorPresenter.Setup(x => x.AssignColors(numberSet)).Returns(colorMapping);
           
            lotteryService.GetLotteryNumber();

            storageService.Verify(x => x.Store(numberSet), Times.Once());
        }

        [Test]
        public void Should_get_numbers_with__expected_color_coding()
        {
            numberGenerator.Setup(x => x.GenerateNumberSet(6, 49)).Returns(numberSet);

            colorPresenter.Setup(x => x.AssignColors(numberSet)).Returns(colorMapping);

            var result = lotteryService.GetLotteryNumber();

            foreach (var item in result)
            {
                item.Value.ShouldBeEquivalentTo(colorMapping[item.Key]);
            }
        }
    }
}
