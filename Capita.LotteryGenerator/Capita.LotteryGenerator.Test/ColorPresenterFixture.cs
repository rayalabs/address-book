﻿using System;
using System.Collections.Generic;
using System.Linq;
using Capita.LotteryGenerator.Service;
using NUnit.Framework;
using FluentAssertions;


namespace Capita.LotteryGenerator.Test
{
    [TestFixture]
    public class ColorPresenterFixture
    {
        private IColorPresenter presenter;

        [SetUp]
        public void Setup()
        {
            presenter = new ColorPresenter();
        }

        [Test]
        public void Should_assign_correct_color_type_to_number()
        {
            var numberSet = new List<int> { 0, 1, 12, 22, 34, 48, 101};

            var result = presenter.AssignColors(numberSet);

            foreach(var number in numberSet)
            {
                if (number >= 0 && number <= 9)
                {
                    result[number].ShouldBeEquivalentTo(ColorType.Red);
                }
                else if (number <= 19)
                {
                    result[number].ShouldBeEquivalentTo(ColorType.Blue);
                }
                else if (number <= 29)
                {
                    result[number].ShouldBeEquivalentTo(ColorType.Green);
                }
                else if (number <= 39)
                {
                    result[number].ShouldBeEquivalentTo(ColorType.Orange);
                }
                else if (number <= 49)
                {
                    result[number].ShouldBeEquivalentTo(ColorType.Purple);
                }
                else
                {
                    result[number].ShouldBeEquivalentTo(ColorType.Undefined);
                }
            }
        }
    }
}
