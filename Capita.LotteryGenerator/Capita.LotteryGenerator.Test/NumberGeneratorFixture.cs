﻿using System;
using System.Linq;
using Capita.LotteryGenerator.Service;
using NUnit.Framework;
using FluentAssertions;

namespace Capita.LotteryGenerator.Test
{
    [TestFixture]
    public class NumberGeneratorFixture
    {
        private INumberGenerator generator;

        [SetUp]
        public void Setup()
        {
            generator = new NumberGenerator();
        }

        [TestCase(1)]
        [TestCase(6)]
        [TestCase(49)]
        public void Should_generate_set_with_given_length(int expectedLength)
        {
            var result = generator.GenerateNumberSet(expectedLength, 49);

            result.Count().ShouldBeEquivalentTo(expectedLength);
        }

        [Test]
        public void Should_generate_set_with_distinct_numbers()
        {
            var result = generator.GenerateNumberSet(6, 49);

            result.Distinct().Count().ShouldBeEquivalentTo(6);
        }

        [TestCase(10)]
        [TestCase(49)]
        public void Should_generate_set_with_numbers_within_limit(int limit)
        {
            var result = generator.GenerateNumberSet(6, limit);
            foreach(var number in result)
            {
                number.Should().BeLessOrEqualTo(limit);
            }
        }
    }
}
