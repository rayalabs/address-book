using System;
using System.Collections.Generic;
using System.Linq;

namespace Capita.LotteryGenerator.Service
{
    public interface INumberGenerator
    {
        IEnumerable<int> GenerateNumberSet(int count, int max);
    }
}