namespace Capita.LotteryGenerator.Service
{
    public enum ColorType
    {
        Undefined,
        Red,
        Blue,
        Green,
        Orange,
        Purple,
    }
}