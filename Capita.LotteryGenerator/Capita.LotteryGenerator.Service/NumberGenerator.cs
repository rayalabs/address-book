﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Capita.LotteryGenerator.Service
{
    public class NumberGenerator : INumberGenerator
    {
        private readonly Random randomNumber;

        public NumberGenerator()
        {
            randomNumber = new Random(DateTime.Now.Millisecond);
        }

        public IEnumerable<int> GenerateNumberSet(int count, int max)
        {
            return Enumerable.Range(1, max).OrderBy(x => randomNumber.Next()).Take(count);
        }
    }
}
