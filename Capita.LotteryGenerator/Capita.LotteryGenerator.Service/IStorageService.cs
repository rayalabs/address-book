using System;
using System.Collections.Generic;

namespace Capita.LotteryGenerator.Service
{
    public interface IStorageService
    {
        void Store(IEnumerable<int> numberSet);
    }
}