using System.Collections.Generic;

namespace Capita.LotteryGenerator.Service
{
    public interface IColorPresenter
    {
        IDictionary<int, ColorType> AssignColors(IEnumerable<int> numberSet);
    }
}