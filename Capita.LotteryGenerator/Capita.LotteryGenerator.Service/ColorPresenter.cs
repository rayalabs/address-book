﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Capita.LotteryGenerator.Service
{
    public class ColorPresenter : IColorPresenter
    {
        private readonly Func<int, ColorType>[] colorMapping = { 
                                                                   (number) => 
                                                                   { 
                                                                       if (number <= 9) 
                                                                            return ColorType.Red; 
                                                                       return ColorType.Undefined; 
                                                                   }, 
                                                                    (number) => 
                                                                   { 
                                                                       if (number <= 19) 
                                                                            return ColorType.Blue; 
                                                                       return ColorType.Undefined; 
                                                                   }, 
                                                                   (number) => 
                                                                   { 
                                                                       if (number <= 29) 
                                                                            return ColorType.Green; 
                                                                       return ColorType.Undefined; 
                                                                   }, 
                                                                   (number) => 
                                                                   { 
                                                                       if (number <= 39) 
                                                                            return ColorType.Orange; 
                                                                       return ColorType.Undefined; 
                                                                   }, 
                                                                    (number) => 
                                                                   { 
                                                                       if (number <= 49) 
                                                                            return ColorType.Purple; 
                                                                       return ColorType.Undefined; 
                                                                   }, 
                                                                    (number) => 
                                                                   { 
                                                                       if (number < 1 || number > 49) 
                                                                            return ColorType.Undefined; 
                                                                       return ColorType.Undefined; 
                                                                   }
                                                               };

        public IDictionary<int, ColorType> AssignColors(IEnumerable<int> numberSet)
        {
            var result = new Dictionary<int, ColorType> { };

            foreach (var number in numberSet)
            {
                if (colorMapping.Any(map => map(number) != ColorType.Undefined))
                {
                    var colorMap = colorMapping.First(map => map(number) != ColorType.Undefined);
                    result.Add(number, colorMap(number));
                }
                else
                {
                    result.Add(number, ColorType.Undefined);
                }
            }

            return result;
        }
    }
}
