﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Capita.LotteryGenerator.Service
{
    public class LotteryService : ILotteryService
    {
        private readonly INumberGenerator numberGenerator;
        private readonly IStorageService storageService;
        private readonly IColorPresenter colorPresenter;

        public LotteryService(INumberGenerator numberGenerator, IStorageService storageService, IColorPresenter colorPresenter)
        {
            this.numberGenerator = numberGenerator;
            this.storageService = storageService;
            this.colorPresenter = colorPresenter;
        }

        public IDictionary<int, ColorType> GetLotteryNumber(int count = 6, int max = 49)
        {
            var lotterySet = numberGenerator.GenerateNumberSet(count, max);

            storageService.Store(lotterySet);

            return colorPresenter.AssignColors(lotterySet);
        }
    }
}
