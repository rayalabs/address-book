﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Capita.LotteryGenerator.Service
{
    public class FileStorageService : IStorageService
    {
        public void Store(IEnumerable<int> numberSet)
        {
            var fileName = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), "CapitaLottertNumbers.txt");

            using (var streamWriter = new StreamWriter(fileName, true))
            {
                streamWriter.WriteLine(string.Format("Lottery set generated at {0} : {1}", DateTime.Now, string.Join(",", numberSet.ToList())));
            } 
        }
    }
}
