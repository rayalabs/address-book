﻿using System;
using System.Collections.Generic;

namespace Capita.LotteryGenerator.Service
{
    public interface ILotteryService
    {
        IDictionary<int, ColorType> GetLotteryNumber(int count = 6, int max = 49);
    }
}
