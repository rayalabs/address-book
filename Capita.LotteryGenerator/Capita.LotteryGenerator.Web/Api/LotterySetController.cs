﻿using System;
using System.Linq;
using System.Web.Http;
using Capita.LotteryGenerator.Service;

namespace Capita.LotteryGenerator.Web.Api
{
    public class LotterySetController : ApiController
    {
        private readonly INumberGenerator numberGenerator;

        public LotterySetController()
        {
            numberGenerator = new NumberGenerator();
        }

        public IHttpActionResult Get(int count = 6, int limit = 49)
        {
            return Ok(numberGenerator.GenerateNumberSet(count, limit));
        }
    }
}
