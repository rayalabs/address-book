﻿using System;
using System.Linq;
using System.Web.Mvc;
using Capita.LotteryGenerator.Service;

namespace Capita.LotteryGenerator.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILotteryService lotteryService;

        public HomeController()
        {
            this.lotteryService = new LotteryService(new NumberGenerator(), new FileStorageService(), new ColorPresenter());
        }
        
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GenerateLotterySet()
        {
            var lotteryNumbers = lotteryService.GetLotteryNumber(6, 49);

            return Json(lotteryNumbers.Select(x => new { Number = x.Key, Color = x.Value.ToString().ToLower() }), JsonRequestBehavior.AllowGet);
        }

    }
}
