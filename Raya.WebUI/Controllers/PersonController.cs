﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Raya.AddressBook.Domain;
using Raya.AddressBook.ServiceLayer;

namespace Raya.AddressBook.Web.Controllers
{
    public class PersonController : ApiController
    {
        private readonly IPersonService service;

        public PersonController()
        {
            service = new PersonService(new PersonRepository());
        }

        public IEnumerable<Person> Get([FromUri] string firstName, string lastName)
        {
            if(!string.IsNullOrWhiteSpace(firstName) || !string.IsNullOrWhiteSpace(lastName))
            {
                return service.Search(firstName, lastName);
            }

            return service.GetAllPersons().ToList();
        }

        public IHttpActionResult Post(Person p)
        {
            var id = service.CreatePerson(p);
            return Ok(new { Id = id } );
        }

        public IHttpActionResult Delete(int id)
        {
            service.RemovePerson(id);
            return Ok();
        }
    }
}
