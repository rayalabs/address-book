﻿using System;
using System.Linq;
using Raya.AddressBook.DataLayer;
using Raya.AddressBook.Domain;

namespace Raya.AddressBook.ServiceLayer
{
    public class PersonRepository : GenericRepository<TestContext, Person>, IPersonRepository
    {
        public Person GetPersonByPhone(string phone)
        {
            return GetAll().FirstOrDefault(x => x.PhoneNumber == phone);
        }
    }
 
    public interface IPersonRepository : IRepository<Person>
    {
        Person GetPersonByPhone(string phone);
    }
}
