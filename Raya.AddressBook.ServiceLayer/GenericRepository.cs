using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using Raya.AddressBook.Domain;

namespace Raya.AddressBook.ServiceLayer
{
    public abstract class GenericRepository<C, T> : IRepository<T> where T : class, IEntity where C : DbContext, new()
    {
        private C _context = new C();

        public C Context
        {
            get
            {
                return _context;
            }

            set
            {
                _context = value ;
            }
        }

        public virtual IQueryable<T> GetAll()
        {
            return _context.Set<T>();
        }

        public virtual IQueryable<T> GetBy(Expression<Func<T, bool>> predicate)
        {
            return _context.Set<T>().Where(predicate);
        }

        public virtual T GetById(int id)
        {
            return _context.Set<T>().SingleOrDefault(x => x.Id == id);
        }

        public virtual void Save()
        {
            _context.SaveChanges();
        }

        public virtual void Insert(T entity)
        {
            _context.Set<T>().Add(entity);
        }

        public virtual void Delete(T entity)
        {
            _context.Set<T>().Remove(entity);
        }
    }
}