﻿using System;
using System.Linq;
using System.Linq.Expressions;
using Raya.AddressBook.Domain;

namespace Raya.AddressBook.ServiceLayer
{
    public interface IRepository<T> where T : class, IEntity
    {
        void Insert(T entity);

        void Delete(T entity);

        T GetById(int id);

        IQueryable<T> GetAll();

        IQueryable<T> GetBy(Expression<Func<T, bool>> predicate);

        void Save();
    }
}
