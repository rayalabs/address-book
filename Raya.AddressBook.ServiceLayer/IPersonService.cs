﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Raya.AddressBook.Domain;

namespace Raya.AddressBook.ServiceLayer
{
    public interface IPersonService
    {
        IEnumerable<Person> GetAllPersons();

        int CreatePerson(Person person);

        void RemovePerson(int id);

        IEnumerable<Person> Search(string firstName, string lastName);
    }

    public class PersonService : IPersonService
    {
        private readonly IPersonRepository repository;

        public PersonService(IPersonRepository repository)
        {
            this.repository = repository;
        }

        public IEnumerable<Person> GetAllPersons()
        {
            return repository.GetAll().ToList();
        }

        public IEnumerable<Person> Search(string firstName, string lastName)
        {
            return repository.GetBy(x => x.FirstName.Contains(firstName) || x.LastName.Contains(lastName)).ToList();
        }

        public int CreatePerson(Person person)
        {
            repository.Insert(person);

            repository.Save();

            return person.Id;
        }

        public void RemovePerson(int id)
        {
            var personToDelete = repository.GetById(id);
            repository.Delete(personToDelete);

            repository.Save();
        }
    }
}
