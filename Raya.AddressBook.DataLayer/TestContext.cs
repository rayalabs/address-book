﻿using System;
using System.Data.Entity;
using System.Linq;
using Raya.AddressBook.Domain;

namespace Raya.AddressBook.DataLayer
{
    public class TestContext : DbContext
    {
        public DbSet<Person> Persons { get; set; }
    }
}
