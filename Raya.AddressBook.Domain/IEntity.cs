namespace Raya.AddressBook.Domain
{
    public interface IEntity
    {
        int Id { get; set; }
    }
}