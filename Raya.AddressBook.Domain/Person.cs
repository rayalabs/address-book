﻿using System;
using System.Linq;

namespace Raya.AddressBook.Domain
{
    public class Person : IEntity
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string PhoneNumber { get; set; }

        public int Id { get; set; }
       
    }
}
